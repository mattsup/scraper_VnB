#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
from MenuChoices import MenuChoices
from GetPage import getpage
from push_poll import PushPoll
from send_slack import SendSlack


## Get main page of the VnB blog
soup_main = getpage("http://danielemarcaccini.com/")


## Get today's menu link from the main page
today = datetime.date.today().strftime("%d-%m-%Y")
## Existing dates for testing :
#today = "20-04-2018"
#today = "03-05-2018"
links = soup_main.find_all('a')

menu_url = ""
for link in links:
    if today in link.get('href'):
        menu_url = link.get('href')
        break

if menu_url == "":
    print("No menu found today.")
    exit()


## Get today's menu page 
soup_menu_today = getpage(menu_url)

## Get today's menu
complete_menu = [x.string for x in soup_menu_today.find(class_="entry-content").find_all("p")]

## Display menu on stdout
#print(complete_menu)


## Sort the menu from the raw complete menu into a structured format :

## The result is a list, in which each element is a custom class MenuChoices
## MenuChoices has a Name (string) like "plat" or "entrée" / a Choice (list) containing today's options
## /and optional side dishes (list)


menu_today = []

for line in complete_menu:
    if ':' in line:
        menu = MenuChoices()
        menu.setName(line.split(':', 1)[0])
        contenu = line.split(':', 1)[1]
        if " e/o " in contenu:
            menu.setChoice(contenu.split(' / ')[0].split(" e/o "))
        else:
            if ' / ' in contenu:
                menu.setChoice(contenu.split(' / ')[0].split(" ou "))
            else:
                menu.setChoice([contenu])
        if ' / ' in contenu:
            if " e/o " in contenu.split(' / ')[1]:
                menu.setSideDish(contenu.split(' / ')[1].split(" e/o "))
            else:
                menu.setSideDish(contenu.split(' / ')[1:])

        for choice in menu.getChoices():
            if menu.getSideDish():
                menu_today.append(f"{menu.getName()} : {choice} + {menu.getSideDish()}")
            else:
                menu_today.append(f"{menu.getName()} : {choice}")

    else:
        menu = MenuChoices()
        menu.setName("Autres")
        menu.setChoice([line])
        menu_today.append(f"{menu.getName()} : {menu.getChoices()}")

## Displays the menu's content for debug
#print("Elements du Menu : #############")
#for i in menu_today:
#    print(i)

## Create survey title
survey_title = f"""Le menu du VnB du {datetime.date.today().strftime("%d-%m-%Y")}"""

## Send the VnB menu to push_poll to create the poll
survey_url = PushPoll(menu_today, "VnB", survey_title)

## Send the survey url to Slack to notify users
SendSlack(f"Voici le <{survey_url} | Nouveau Menu> du jour !")

