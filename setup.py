from setuptools import setup

setup(name='Scraper_VnB',
    version='1.0',
    description='Scraps VnB',
    url='',
    author='',
    packages=find_packages(),

    install_requires=[
        'push_poll',
        'send_slack',
    ],
    dependency_links=[
        'https://gitlab.com/mattsup/push_poll.git#egg=push_poll',
        'https://gitlab.com/mattsup/send_slack.git#egg=send_slack'
    ]
)

