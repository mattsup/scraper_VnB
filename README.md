# scraper_vnb

Script made to scrap the content of a restaurant's menu named VnB. Gets the menu and structure it, then send it to push_poll to create a poll out of it.

# Usage

Just execute it, by default it gets the current date to check if there is a VnB menu available today.

