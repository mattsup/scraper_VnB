#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests


def getpage(website):
    main = requests.get(website)
    page_main = main.text
    soup_main = BeautifulSoup(page_main, 'html.parser')
    return soup_main
