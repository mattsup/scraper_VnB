#!/usr/bin/env python
# -*- coding: utf-8 -*-


class MenuChoices:

    __name = None
    __choices = []
    __sidedish = []

    def setName(self, name):
        self.__name = name

    def setChoice(self, choices):
        self.__choices = choices

    def setSideDish(self, sidedish):
        self.__sidedish = sidedish

    def getChoices(self):
        return(self.__choices)

    def getName(self):
        return(self.__name)

    def getSideDish(self):
        return(self.__sidedish)

## Display function for debug
    def display(self):
        print("----")
        print("Type: " + self.__name)
        for any in self.__choices:
            print("Choix: " + any)
        if self.__sidedish:
            for any in self.__sidedish:
                print("Accompagnements: " + any)

